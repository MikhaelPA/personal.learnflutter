import 'dart:convert';

import 'package:intl/intl.dart';

// List<String> countriesName(List<CountryCovid19KeyVaue> countries) {
//   final countryList = <String>[];
//   countries.country.map((item){
//     countryList.add(item.countryName);
//   });
//   return countryList;
// }

// List<Map<String, CountryCovid19>> countriesFromJson(String str) {
//   final jsonData = json.decode(str);
//   // final countries = <CountryCovid19>[];
//   // for (var item in jsonData) {
//   //   countries.add(CountryCovid19.fromJson(item));
//   // }
//   final sample = [
//     {
//       "country": "China",
//       "cases": 81008,
//       "todayCases": 41,
//       "deaths": 3255,
//       "todayDeaths": 7,
//       "recovered": 71740,
//       "active": 6013,
//       "critical": 1927,
//       "casesPerOneMillion": 56
//     },
//     {
//       "country": "Italy",
//       "cases": 47021,
//       "todayCases": 0,
//       "deaths": 4032,
//       "todayDeaths": 0,
//       "recovered": 5129,
//       "active": 37860,
//       "critical": 2655,
//       "casesPerOneMillion": 778
//     }
//   ];
//   print(sample[0].keys('country'));

//   return fromJson;
// }

// List<CountryCovid19KeyVaue> countriesFromJson(String str) {
//   final jsonData = json.decode(str);
//   final sample = [
//     {
//       "country": "China",
//       "cases": 81008,
//       "todayCases": 41,
//       "deaths": 3255,
//       "todayDeaths": 7,
//       "recovered": 71740,
//       "active": 6013,
//       "critical": 1927,
//       "casesPerOneMillion": 56
//     },
//     {
//       "country": "Italy",
//       "cases": 47021,
//       "todayCases": 0,
//       "deaths": 4032,
//       "todayDeaths": 0,
//       "recovered": 5129,
//       "active": 37860,
//       "critical": 2655,
//       "casesPerOneMillion": 778
//     }
//   ];
//   return List<CountryCovid19KeyVaue>.fromJson(sample);
// }

// class List<CountryCovid19KeyVaue> {
//   Set<CountryCovid19KeyVaue> country;

//   List<CountryCovid19KeyVaue>({country});

//   factory List<CountryCovid19KeyVaue>.fromJson(List<Map<String, dynamic>> json) {
//     final countryList = <CountryCovid19KeyVaue>[];
//     json.map((item) {
//       final data = new CountryCovid19KeyVaue(
//           countryName: item['country'],
//           countryData: CountryCovid19.fromJson(item));
//       countryList.add(data);
//     });
//     print(countryList);
//     return new List<CountryCovid19KeyVaue>(country: countryList);
//   }
// }

class CountryCovid19KeyVaue {
  String countryName;
  CountryCovid19 countryData;

  CountryCovid19KeyVaue({countryName, countryData});
}

CountryCovid19 countryFromJson(String str) {
  final jsonData = json.decode(str);
  return CountryCovid19.fromJson(jsonData);
}

class CountryCovid19 {
  final _numFormat = new NumberFormat('#,###', 'en_US');

  String country;
  int cases;
  int todayCases;
  int deaths;
  int todayDeaths;
  int recovered;
  int active;
  int critical;

  String get casesStr => _numFormat.format(cases);
  String get todayCasesStr => _numFormat.format(todayCases);
  String get deathsStr => _numFormat.format(deaths);
  String get todayDeathsStr => _numFormat.format(todayDeaths);
  String get recoveredStr => _numFormat.format(recovered);
  String get activeStr => _numFormat.format(active);
  String get criticalStr => _numFormat.format(critical);

  CountryCovid19(
      {
        this.country,
      this.cases,
      this.todayCases,
      this.deaths,
      this.todayDeaths,
      this.recovered,
      this.active,
      this.critical});

  factory CountryCovid19.fromJson(Map<String, dynamic> json) {
    print(json['country']);
return new CountryCovid19(
          country: json['country'],
          cases: json['cases'],
          todayCases: json['todayCases'],
          deaths: json['deaths'],
          todayDeaths: json['todayDeaths'],
          recovered: json['recovered'],
          active: json['active'],
          critical: json['critical']);
  }
     
}
