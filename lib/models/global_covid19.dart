import 'dart:convert';

import 'package:intl/intl.dart';

GlobalCovid19 globalFromJson(String str) {
  final jsonData = json.decode(str);
  return GlobalCovid19.fromJson(jsonData);
}

class GlobalCovid19 {
  final _numFormat = new NumberFormat('#,###', 'en_US');

  int cases;
  int deaths;
  int recovered;

  GlobalCovid19({this.cases, this.deaths, this.recovered});

  String get casesStr => _numFormat.format(cases);
  String get deathsStr => _numFormat.format(deaths);
  String get recoveredStr => _numFormat.format(recovered);

  factory GlobalCovid19.fromJson(Map<String, dynamic> json) =>
      new GlobalCovid19(
        deaths: json['deaths'],
        recovered: json['recovered'],
        cases: json['cases'],
      );
}
