import 'package:first_app/views/words/word_delete.dart';
import 'package:flutter/material.dart';
import 'package:english_words/english_words.dart';

class SavedWords extends StatelessWidget {
  final Set<WordPair> saved;
  final TextStyle biggerFont;
  SavedWords(this.saved, this.biggerFont);

  @override
  Widget build(BuildContext context) {
    final tiles = saved.map((WordPair pair) {
      return Dismissible(
        key: ValueKey(pair.asPascalCase),
        direction: DismissDirection.startToEnd,
        onDismissed: (direction) {
          saved.remove(pair);
        },
        confirmDismiss: (direction) async {
          final result =
              await showDialog(context: context, builder: (_) => WordDelete());
          return result;
        },
        background: Container(
          color: Colors.red,
          padding: EdgeInsets.only(left: 16),
          child: Align(
            child: Icon(Icons.delete, color: Colors.white),
            alignment: Alignment.centerLeft,
          ),
        ),
        child: ListTile(
          title: Text(
            pair.asPascalCase,
            style: biggerFont,
          ),
          // trailing: Icon(Icons.close),
          // onTap: (){
          //   setState(() {
          //     _saved.remove(pair);

          //   });
          // },
        ),
      );
    });
    final divided =
        ListTile.divideTiles(tiles: tiles, context: context).toList();

    return Scaffold(
      appBar: AppBar(title: Text('Saved Suggestions')),
      body: ListView(children: divided),
    );
  }
}
