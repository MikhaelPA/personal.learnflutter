import 'package:first_app/models/api_response.dart';
import 'package:first_app/models/country_covid19.dart';
import 'package:flutter/material.dart';

class CountryView extends StatefulWidget {
  final APIResponse<CountryCovid19> apiResponse;
  CountryView(this.apiResponse);

  @override
  _CountryViewState createState() => _CountryViewState(this.apiResponse);
}

class _CountryViewState extends State<CountryView> {
  final _styleTitleFont = const TextStyle(fontSize: 20.0, color: Colors.white);
  final _styleSubsFont = const TextStyle(fontSize: 45.0, color: Colors.white);
  static const _iconSize = 55.0;

  final APIResponse<CountryCovid19> apiResponse;
  _CountryViewState(this.apiResponse);

  List<String> _countries = <String>[];
  //String _selectedCountry = "indonesia";
  @override
  void initState() {
    // if (apiResponse.error == false) {
    //   _countries = ['china','indonesia'];//countriesName(apiResponse.data);
    // }
    super.initState();
  }

  List<DropdownMenuItem> buildDropdownItem() {
    return _countries.map((String item) {
      return DropdownMenuItem<String>(
        value: item,
        child: Text(item),
      );
    }).toList();
  }

  @override
  Widget build(BuildContext context) {
    // CountryCovid19KeyVaue _selected = apiResponse
    // .data.where((item) => item.countryName == _selectedCountry)
    // .first ?? CountryCovid19KeyVaue(countryName: Text('text'), countryData: CountryCovid19(cases: 0)); 
    //print(_selected);
    return ListView(
      children: <Widget>[
        // DropdownButton<String>(
        //   items: buildDropdownItem(),
        //   onChanged: (String newSelected) {
        //     setState(() {
        //       _selectedCountry = newSelected;
        //     });
        //   },
        // ),
        Card(
          margin: EdgeInsets.all(15.0),
          color: Colors.orange,
          child: ListTile(
            // contentPadding:
            //     EdgeInsets.symmetric(vertical: 50.0, horizontal: 30.0),
            // leading: Icon(
            //   Icons.vpn_lock,
            //   color: Colors.white,
            //   size: _iconSize,
            // ),
            title: Text(
              apiResponse.data.country,//_selected.countryName,
              style: _styleTitleFont,
            ),
            subtitle: Text(
              apiResponse.data.casesStr,
              style: _styleSubsFont,
            ),
          ),
        ),
      ],
    );
  }
}
