import 'package:first_app/models/api_response.dart';
import 'package:first_app/models/country_covid19.dart';
import 'package:first_app/models/global_covid19.dart';
import 'package:first_app/services/covid19_service.dart';
import 'package:first_app/views/country_view.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

import 'global_view.dart';

class Covid19Dashboard extends StatefulWidget {
  @override
  _Covid19DashboardState createState() => _Covid19DashboardState();
}

class _Covid19DashboardState extends State<Covid19Dashboard> {
  Covid19Service get service => GetIt.I<Covid19Service>();

  APIResponse<GlobalCovid19> _apiResponseGlobal;
  APIResponse<CountryCovid19> _apiResponseCountry;
  bool _isLoading = false;
  int _currentIndex = 0;

  @override
  void initState() {
    _fetchData();
    super.initState();
  }

  _fetchData() async {
    setState(() {
      _isLoading = true;
    });

    _apiResponseGlobal = await service.getGlobalData();
    _apiResponseCountry = await service.getCountryData();

    setState(() {
      _isLoading = false;
    });
  }

  void _refreshData() {
    _fetchData();
  }

  @override
  Widget build(BuildContext context) {
    final tabs = [
      _buildGlobal(),
      _buildCountry(),
    ];

    return Scaffold(
      appBar: AppBar(
        title: Text('COVID-19'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.refresh),
            onPressed: _refreshData,
          )
        ],
      ),
      body: tabs[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentIndex,
        type: BottomNavigationBarType.fixed,
        iconSize: 40.0,
        backgroundColor: Colors.white,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.map),
            title: Text('Global'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.pin_drop),
            title: Text('Country'),
          ),
          // BottomNavigationBarItem(
          // icon: Icon(Icons.info),
          // title: Text('Info'),
          // )
        ],
        onTap: (index) {
          setState(() {
            _currentIndex = index;
          });
        },
      ),
    );
  }
  Widget _buildGlobal() {
    return Builder(
      builder: (_) {
        if (_isLoading) {
          return Center(child: CircularProgressIndicator());
        }

        if (_apiResponseGlobal.error) {
          return Center(
            child: Text(_apiResponseGlobal.errorMessage),
          );
        }

        return GlobalView(_apiResponseGlobal);
      },
    );
  }

  Widget _buildCountry() {
    return Builder(
      builder: (_) {
        if (_isLoading) {
          return Center(child: CircularProgressIndicator());
        }

        if (_apiResponseCountry.error) {
          return Center(
            child: Text(_apiResponseCountry.errorMessage),
          );
        }

        return CountryView(_apiResponseCountry);
      },
    );
  }
}
