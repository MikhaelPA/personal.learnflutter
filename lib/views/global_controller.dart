import 'package:first_app/models/api_response.dart';
import 'package:first_app/models/global_covid19.dart';
import 'package:first_app/services/covid19_service.dart';
import 'package:first_app/views/global_view.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

class GlobalWidget extends StatefulWidget {
  @override
  _GlobalWidgetState createState() => _GlobalWidgetState();
}

class _GlobalWidgetState extends State<GlobalWidget> {
  Covid19Service get service => GetIt.I<Covid19Service>();
  APIResponse<GlobalCovid19> _apiResponse;

  bool _isLoading = false;
  @override
  void initState() {
    _fetchData();

    super.initState();
  }

  _fetchData() async {
    setState(() {
      _isLoading = true;
    });

    _apiResponse = await service.getGlobalData();

    setState(() {
      _isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Builder(
      builder: (_) {
        if (_isLoading) {
          return Center(child: CircularProgressIndicator());
        }

        if (_apiResponse.error) {
          return Center(
            child: Text(_apiResponse.errorMessage),
          );
        }

        return GlobalView(_apiResponse);
      },
    );
  }
}
